from sklearn.externals import joblib
import seaborn as sns
sns.set(style="white",font_scale=1.5)

from IPython.display import HTML
from IPython.display import display

# Taken from https://stackoverflow.com/questions/31517194/how-to-hide-one-specific-cell-input-or-output-in-ipython-notebook
tag = HTML('''<script>
code_show=false; 
function code_toggle() {
    if (code_show){
        $('div.cell.code_cell.rendered.selected div.input').hide();
    } else {
        $('div.cell.code_cell.rendered.selected div.input').show();
    }
    code_show = !code_show
} 
$( document ).ready(code_toggle);
</script><span style="font-family: wingdings; font-size: 200%;">&#10004;</span>
To show/hide the answer, click <a href="javascript:code_toggle()">here</a>.''')

#to hide all answer cells, containing display(models.tag)
hideAnswers = HTML('''<script>
var all_cells = Jupyter.notebook.get_cells();
var num_cells = all_cells.length;
for (var i = 0; i < num_cells; i++){
    var current_cell = Jupyter.notebook.get_cell(i);
    var current_cell_code = current_cell.get_text();
    if (current_cell_code.includes("display(models.tag)") && current_cell_code.indexOf("%%") <= -1){
        current_cell.execute();
        current_div = current_cell.element[0]
        current_input = current_div.children[0]
        $(current_input).hide()
    }
}</script>''')

print("Imported models functions")

def deleteColumns(columns_to_delete, df):
    for i in columns_to_delete:
        df = df.drop(i, axis=1)
    return df

def saveModel(model, filename):
    '''Saving a trained model to pickle file
    args:
    1. trained model variable
    2. filename for saving, full path'''
    joblib.dump(model, filename)
    print ("Saved model to pickle:"+str(filename))
                
#loads a trained model from a pickle file                
def loadModel(filename): 
    '''Loading a trained model from pickle file
    args: 
    filepath - file path and name of pickle file'''
    model = joblib.load(filename)
    print ("Loaded model from pickle:"+str(filename))
    return model