from sklearn.gaussian_process.kernels import Kernel
from rdkit import Chem
from rdkit.Chem import MACCSkeys
from rdkit.Chem import rdmolops
from rdkit.Chem.Fingerprints import FingerprintMols
import rdkit.Chem.rdMolDescriptors
from rdkit.Chem import AtomPairs
#from rdkit.DataStructs import FingerprintSimilarity
from rdkit import DataStructs
import numpy as np

def toFingerprint(inchi, fingerprint=lambda m: rdkit.Chem.rdMolDescriptors.GetMorganFingerprintAsBitVect(m, radius=3 )):
    ret = Chem.MolFromInchi(inchi, sanitize=False)
    error = Chem.SanitizeMol(ret, sanitizeOps=Chem.rdmolops.SanitizeFlags.SANITIZE_ALL , catchErrors=True)
    return fingerprint(ret) if (Chem.rdmolops.SanitizeFlags.SANITIZE_NONE==error) else None

class MoleculeKernel(Kernel):
    """MoleculeKernel
    Measures the similarity between molecules
    TODO revise this to accept fingerprints
    Parameters
    ----------
    fingerprint: the function used to digest a molecule to a bitstring
         Default: Chem.rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect
         See http://www.rdkit.org/Python_Docs/rdkit.Chem.rdMolDescriptors-module.html#GetHashedAtomPairFingerprintAsBitVect 
    similarity: the measure of similarity of two keys
         Default: rdkit.DataStructs.cDataStructs.TanimotoSimilarity
         See http://www.rdkit.org/docs/api/rdkit.DataStructs-module.html#FingerprintSimilarity 
    """
        
    def __init__(self, fingerprint=Chem.rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect, 
                 similarity=rdkit.DataStructs.cDataStructs.TanimotoSimilarity):
        self._fingerprint = fingerprint
        self._similarity = similarity
        return
    
    # note that these fingerprints have optional arguments
    # an exhaustive set of tests requires a parameter sweep
    FINGERPRINTS = [  
        Chem.rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect,
        rdmolops.PatternFingerprint, 
        lambda m: Chem.rdMolDescriptors.GetMorganFingerprintAsBitVect(m, 7),
        # remining ones not so good
        rdmolops.LayeredFingerprint, 
        rdmolops.RDKFingerprint,            
        Chem.rdMolDescriptors.GetHashedTopologicalTorsionFingerprintAsBitVect,
        Chem.rdMolDescriptors.GetMACCSKeysFingerprint,
        FingerprintMols.FingerprintMol,   
]
    
    SIMILARITIES = [ e[1] for e in DataStructs.similarityFunctions if "Sokal" not in e[0]]
    
    def __repr__(self):
        return 'Molecule Kernel: '+self._fingerprint.__name__ +' with '+self._similarity.__name__
    @property
    def anistropic(self):
        return False 
    def is_stationary(self):
        """Returns whether the kernel is stationary. """
        return False
    
    def diag(self, X):
        # TODO normalise this kernel
        return np.apply_along_axis(self, 1, X)[:, 0]
    
    def __call__(self, X, Y=None, eval_gradient=False):
        """Return the kernel k(X, Y).
        
        Parameters
        ----------
        X : array, shape (n_samples_X, 1)
            Left argument of the returned kernel k(X, Y)
        Y : array, shape (n_samples_Y, 1), (optional, default=None)
            Right argument of the returned kernel k(X, Y). If None, k(X, X)
            if evaluated instead.
        eval_gradient : bool (optional, default=False)
            Must be false.
        Returns
        -------
        K : array, shape (n_samples_X, n_samples_Y)
            Kernel k(X, Y)
        """
        if eval_gradient:
            raise Value_Error('Cannot evaluate gradients because molecules are not a continuous space.')
        if Y is None:
            Y = X
        X = [self._fingerprint(x) for x in X]
        Y = [self._fingerprint(y) for y in Y]
        K = np.zeros([len(X), len(Y)], dtype=np.float64)  
        for i in range(0, len(X)):
            for j in range(0, len(Y)):
                K[i, j] =  DataStructs.FingerprintSimilarity(X[i], Y[j], metric=self._similarity)
        return np.array(K, dtype=np.float64)
        
    
# end of class Kernel
assert MoleculeKernel()(
  [Chem.MolFromSmiles('C')],
  [Chem.MolFromSmiles('CC')]
) == [
    rdkit.DataStructs.cDataStructs.DiceSimilarity(
        Chem.rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect(Chem.MolFromSmiles('C')), 
        Chem.rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect(Chem.MolFromSmiles('CC'))
        )
]
gram = MoleculeKernel()(
  [Chem.MolFromSmiles('C')],
  [Chem.MolFromSmiles('C'), Chem.MolFromSmiles('CC')]
)
assert (1,2) == np.shape(gram)
