from rdkit import Chem
from rdkit.Chem import Descriptors
import pandas as pd
from rdkit.DataStructs.cDataStructs import ConvertToNumpyArray
import numpy
print("Imported custom rdkit functions")

###get the RDKit molecule
def getMolecule(row):
    if pd.notnull(row['SMILES']):
        m = Chem.MolFromSmiles(row['SMILES'],sanitize=False)
        if pd.notnull(m):
            error = Chem.SanitizeMol(m,catchErrors=True)
            if Chem.SANITIZE_NONE == error:
                return m
    return None


###DESCRIPTOR FUNCTIONS-----------------------------------------------------------------
def describe(descriptor, molecules):
    return list(map( lambda m: getattr(Descriptors, descriptor)(m), molecules))

assert [8] == describe('NumValenceElectrons', [Chem.MolFromSmiles('C')])

# add a descriptor that seems useful
def FractionAromaticAtom(m): 
    return float( sum(1 for a in m.GetAtoms() if a.GetIsAromatic() ) ) / Descriptors.HeavyAtomCount(m)


assert 1.0 == FractionAromaticAtom(Chem.MolFromSmiles('c1ccccc1'))  
setattr(Descriptors, 'FractionAromaticAtom', FractionAromaticAtom)

def FractionAromaticValenceElectrons(m): 
    return float(sum(3 for b in m.GetBonds()if b.GetIsAromatic() )) / Descriptors.NumValenceElectrons(m)


assert 0.6 == FractionAromaticValenceElectrons(Chem.MolFromSmiles('c1ccccc1'))  
setattr(Descriptors, 'FractionAromaticValenceElectrons', FractionAromaticValenceElectrons)

def deltaEState(m): 
    return float(Descriptors.MaxEStateIndex(m) - Descriptors.MinEStateIndex(m))


assert 0.0 == deltaEState((Chem.MolFromSmiles('c1ccccc1')))
setattr(Descriptors, 'deltaEState', deltaEState)

def getValues(descriptor, molecules):
    series = describe(descriptor, molecules)
    return np.unique(series)

allDescriptors =  [ d
	for d in dir(Descriptors) 
	if callable(getattr(Descriptors, d)) and not d in [
		'_isCallable', '_setupDescriptors', '_ChargeDescriptors', '_test',
		# the following can return NaN:
		'MaxAbsPartialCharge', 'MaxPartialCharge', 'MinAbsPartialCharge', 'MinPartialCharge' 
	]
]

# Make the independent variable array, as Scikit expects it
def makeX(descriptors, molecules):
    """Gathers descriptors for molecules
    
    Keyword arguments:
    descriptors -- descriptors to be returned
    molecules -- molecules to be used
    """
    x= pd.DataFrame()
    for d in descriptors:
        x[d] = describe(d, molecules)
    return x.values

#Gathering molecule fingerprints and converting to numpy array
def getFingerprintArray(data):
    fps = []
    for i, row in data.iterrows():
        fp = Chem.rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect(row["molecule"])
        arr = numpy.zeros((1,))
        ConvertToNumpyArray(fp,arr)
        fps.append(arr)
    return fps