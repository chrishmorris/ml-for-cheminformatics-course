{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"http://www.stfc.ac.uk/\"> <img src=\"../../img/STFCLargeColour.jpg\" width=\"50%\" align=\"left\"/></a>\n",
    "<a href=\"https://www.addopt.org/\"><img src=\"../../img/ADDoPT-logo-MAIN-with-text.png\" width=\"300\" height=\"200\" align=\"right\"/></a>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Background: How Machine Learning theory differs from Classical Statistics\n",
    "\n",
    "This is optional material for the course: reading it is not required to understand the other material.\n",
    "\n",
    "In the 1920s and 1930s, Fisher in the UK and Kolmogorov in the Soviet Union were both working on the foundations of statistics. Kolmogorov engaged with some deep mathematical problems, which were not solved until the 1980s, giving rise to the methods of machine learning. Fisher took some pragmatic decisions, which allowed practical progress in statistics. Some of the differences are described below.\n",
    "\n",
    "## Asymptotic methods versus uniform convergence\n",
    "The Law of Large Numbers states that as the sample size tends to infinity, the frequency of an observed event tends to its probability. Similarly, the mean of a statistic over the sample tends to its expectation. The use of Fisher's methods depend on these results. \n",
    "\n",
    "However, these results have limitations. Suppose our set of models has a parameter k, and the model is:\n",
    "-   $y = k^2$ with probability $1/k$\n",
    "-   $y = 0$ with probability $1-1/k$\n",
    "\n",
    "So $E(y) = k$. Then for small k, the mean of the sample is a good estimator of k. But if k is very large, then our sample probably consists entirely of zeroes, and we cannot estimate k. Kolmogorov investigated criteria for *uniform convergence*, bounds on the probability of error that apply across a whole set of models. His programme lead to discovery of criteria for explaining why this set of models, with unbounded k, is unsuitable for machine learning. One of these is the Vapnik-Chernovenko Dimension. Another is a precise definition of what makes a set of models too fat-tailed for uniform convergence.\n",
    "\n",
    "## Parametric versus non-parametric methods\n",
    "Many natural processes produce results with a normal distribution. Some produce results with an exponential distribution, or some other analytically simple distribution. Fisher's methods begin by assuming the class of distributions, then finding the maximum likelihood estimate of the parameters, e.g. the mean and variance of a normal distribution. The theorems of classical statistics bound the probable error in the estimated parameters, provided that the assumption is valid that the process does conform to the distribution.\n",
    "\n",
    "Bayesian methods make a similar assumption. The true distribution is assumed to come from a set of models, with a parameter distributed according to the prior distribution. If the real distribution is not in the set, then the theoretical foundations of Bayesian methods are unclear. That is why our [Gaussian Process Regression](GPR.ipynb) produced odd results. However you view the theoretical foundations, these methods can be useful for some problems.\n",
    "\n",
    "Machine learning methods also start by choosing a class of models and (often) by fitting parameters to find the best fitting model in the class. However, the theorems of Kolmogorov and his followers bound not only the error between the learnt model and the best model, but also the error between the best model in class and the real distribution.\n",
    "\n",
    "## Regularization\n",
    "In classical statistics the default loss function is the likelihood of the sample, and the preferred estimate of the distribution parameter is the one that minimizes this, known as the *Maximum Likelihood Estimator*. However, MLEs are often biased. Suppose that there are some tickets in a box, numbered from $1$ to unknown $N$. A ticket is drawn, and is numbered $x$. The MLE estimate of $N$ is $x$, but the unbiased estimate is $2x-1$.\n",
    "\n",
    "In machine learning, loss functions include a regularization term. In general, regularized estimators are proven to be  unbiased. The Representer Theorem proved by Grace Wahba and its generalization by Schölkopf and Smola, show that that a wide range of regularized methods are equivalent to support vector machines.\n",
    "\n",
    "## The difficulty of estimating probability density\n",
    "Fisher's methods amount to using the empirical data to estimate the probability density function (pdf). The Glivenko-Cantorelli Theorem states that an empiricial estimate converges in probability to the cumulative distribution function (cdf)- and it does so exponentially fast. However, the pdf is the differential of the pdf. Two functions that are close together can have differentials that are very different. In general, there is no way to estimate a pdf.\n",
    "\n",
    "## Bayesian methods\n",
    "A third paradigm is the Bayesian one. It is more computationally intensive than SVMs, but less than neural nets. It needs programming skills and understanding of mathematics, perhaps more than the methods discussed in this course.\n",
    "<img src=\"http://www.lecun.org/gallery/libpro/20011121-allyourbayes/dsc01228-02-h.jpg\" style=\"width: 50%\" />\n",
    "Vladimir Vapnik in 2001\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "Below there is an implementation of the ticket in a box problem. Implement a maximal likelihood estimator, a regularized estimator, and a Bayesian estimator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 120,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 120,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import random, math\n",
    "\n",
    "# each time this is called, it returns an example of the ticket in a box problem\n",
    "def get_distribution():\n",
    "    n = math.floor( 1+random.expovariate(0.1) )\n",
    "    return lambda : random.randrange(1, n)\n",
    "\n",
    "distribution = get_distribution()\n",
    "sample = distribution()\n",
    "sample"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Further reading\n",
    "\n",
    "For more information on the topics covered here:\n",
    "    \n",
    "- Vapnik, *The Nature of Statistical Learning Theory*\n",
    "- [PyMC3](http://docs.pymc.io/) toolkit for Bayesian methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[Up](../Welcome.ipynb)<br> [Lessons Learnt](LessonsLearnt.ipynb) \n",
    "\n",
    "Copyright STFC 2018"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
