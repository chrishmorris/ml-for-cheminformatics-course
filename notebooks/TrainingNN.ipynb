{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"http://www.stfc.ac.uk/\"> <img src=\"../img/STFCLargeColour.jpg\" width=\"50%\" align=\"left\"/></a>\n",
    "<a href=\"https://www.addopt.org/\"><img src=\"../img/ADDoPT-logo-MAIN-with-text.png\" width=\"300\" height=\"200\" align=\"right\"/></a>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training Neural Networks\n",
    "\n",
    "Now we have seen a  [neuron](Neuron.ipynb), let's look at how they are trained.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import statements\n",
    "import sys\n",
    "sys.path.append(\"../lib/\")\n",
    "import models\n",
    "\n",
    "models.hideAnswers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Back propagation / Gradient Descent for a single neuron\n",
    "\n",
    "In backward propagation, we are looking to optimise a cost function, such that the error between the predicted output and the actual output is minimal.\n",
    "\n",
    "During each backpropagation iteration, the weights and bias are adjusted in order to do this.\n",
    "\n",
    "We can set the size of the change in the parameters using the learning rate, $\\alpha$, which we will set to 0.1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The empirical cost function for all training examples is defined as:\n",
    "\n",
    "$$C(\\hat{y},y) = \\frac{1}{m}\\sum{L(y,\\hat{y})}$$\n",
    "\n",
    "For logistic regression, the loss function for a single training example is:\n",
    "\n",
    "$$L(y,\\hat{y}) = -(\\hat{y}\\cdot\\log(y) + (1-\\hat{y})\\cdot\\log(1-y))$$\n",
    "\n",
    "\n",
    "Recall that $\\hat{y}$ is the actual output, whilst $y$ is the probability of being a drug given $W$ and $b$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "During an update step, we determine the gradient of the cost with respect to the parameters $W$ and $b$:\n",
    "\n",
    "$$\\frac{dC}{dW}$$ <br>$$\\frac{dC}{db}$$\n",
    "\n",
    "which here is denoted as $dW$ and $db$ respectively.\n",
    "\n",
    "When it comes to updating each parameter, we use the learning rate ($\\alpha$) and the gradient ($dW,db$):\n",
    "\n",
    "$$W = W - \\alpha \\cdot dW$$\n",
    "\n",
    "$$b = b - \\alpha \\cdot db$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training the model\n",
    "\n",
    "As we require multiple update steps to allow our parameters to find the global minimum for the cost function, it is reasonable to allow a few thousand iterations (epochs) during training. As model proceeds through iterations the cost is reduced and the parameters are updated as they get closer to the optimal parameters.\n",
    "\n",
    "<img src=\"http://neuralnetworksanddeeplearning.com/images/regularized1.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After training, one neuron has the same expressive power as Logistic Regression. With more layers, the network can learn more complex functions, and so can acheive a better fit than Logistic Regression."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Momentum\n",
    "This is *Stochastic Gradient Descent*. Newton's method speeds up training by using the second differential. Sometimes it is used to train neural nets faster than stochastic gradient descent can do. A simpler alternative is to use momentum: the training method keeps a running total of updates, and continues the descent in the direction previous found. This often works as well as higher order methods. In these examples, we use *Nesterov momentum* for these reasons.\n",
    "\n",
    "Neural nets are often improved with a final, momentum-free fine tuning, performed with a very low learning rate.\n",
    "\n",
    "\n",
    "<div class=\"alert alert-warning\" role=\"alert\">\n",
    "<b>Exercise</b> \n",
    "The [druglikeness network](DrugNN.ipynb) uses SCD with Nesterov Momentum. Try a higher or lower learning rate.\n",
    "Try an [alternative optimiser](https://keras.io/optimizers/)\n",
    "</li>\n",
    "</div>\n",
    "\n",
    "\n",
    "\n",
    "## Dropout\n",
    "\n",
    "A neural network has a weight parameter for each link between neurons, and a threshold for each neuron. With this large set of parameters, the potential for overfitting is large. One strategy for avoiding this is regularization. There is an exercise below to try this. \n",
    "\n",
    "When we were building Random Forests, we saw another strategy for avoiding overfitting: we built many models, each using a random subset of the training set, and used the consensus prediction. Unfortunately, building a neural net is computationally intensive, so building an ensemble is not an attractive option. \n",
    "\n",
    "The dropout strategy addresses this. At the start of each epoch, a random subset of the hidden neurons are turned off, i.e. the outputs are fixed to 0. These neurons do not participate in this epoch. This strategy has proved to be very effective.\n",
    "\n",
    "\n",
    "<div class=\"alert alert-warning\" role=\"alert\">\n",
    "<b>Exercise</b> \n",
    "<ol>\n",
    "    <li>The [druglikeness network](DrugNN.ipynb) uses dropout. What happens without dropout?</li>\n",
    "    <li>It does not use dropout on the input layer. What happens if you introduce 20% dropout of the inputs?</li>\n",
    "    <li>It also regularizes the weights. Try an [alternative regularizer](https://keras.io/regularizers/).</li>\n",
    "</ol>\n",
    "</div>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Further reading\n",
    "\n",
    "- [Srivastava et al., *Dropout: A Simple Way to Prevent Neural Networks from Overfitting*](https://www.cs.toronto.edu/~hinton/absps/JMLRdropout.pdf)\n",
    "- [Theano neural networks](https://www.analyticsvidhya.com/blog/2016/04/neural-networks-python-theano/)\n",
    "- [Theano tutorial](http://deeplearning.net/software/theano/tutorial/examples.html)\n",
    "- [Second order training](https://github.com/boulanni/theano-hf)\n",
    "- [Sutskever et al. *On the importance of initialization and momentum in deep learning*](http://www.cs.toronto.edu/~fritz/absps/momentum.pdf)\n",
    "\n",
    "[Previous](Neuron.ipynb)  [Next](./ANN/ANNR.ipynb)<br>\n",
    "\n",
    "Copyright STFC 2018"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
