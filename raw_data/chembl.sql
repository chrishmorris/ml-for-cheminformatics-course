
-- drugs, herbicides, fungicides, and insecticides
copy (
with molecule as (
   (select molregno from molecule_frac_classification)
   union(select molregno from molecule_hrac_classification)
   union(select molregno from molecule_irac_classification)
   union (select molregno from molecule_dictionary where max_phase>0)
)
select mol_frac_id as fungicide_moa, 
    mol_hrac_id as herbicide_moa, 
	mol_irac_id as insectide_moa, 
    max_phase, 
    standard_inchi
from compound_structures as cs
    left join molecule_frac_classification as f on cs.molregno=f.molregno 
    left join molecule_hrac_classification as h on cs.molregno=h.molregno 
    left join molecule_irac_classification as i on cs.molregno=i.molregno, 
	molecule, molecule_dictionary
where cs.molregno=molecule.molregno
     and cs.molregno=molecule_dictionary.molregno
) to 'C:\git\ml_course\raw_data\chembl_classes.csv' csv header;

-- see ftp://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/schema_documentation.html
