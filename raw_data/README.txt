Location of all raw data files used as input to modelling and lessons
Thanks to Bergstrom et al. for melting point data
Bergstrom, C. A. S.; Norinder, U.; Luthman, K.; Artursson, P. Molecular Descriptors Influencing Melting Point and Their Role in Classification of Solid Drugs. J. Chem. Inf. Comput. Sci.; (Article); 2003; 43(4); 1177-1185


See http://cheminformatics.org/datasets/

Thansk to Jarmo Huuskonen for solubility.csv. See:
Huuskonen Aequeous Solubility Dataset  

Jarmo Huuskonen, J. Chem. Inf. Comput. Sci., 2000, 40, 773-777. 


Thanks to Igor Tetko for the logP.csv
See http://dx.doi.org/10.1002/jps.21494 and https://ochem.eu/

